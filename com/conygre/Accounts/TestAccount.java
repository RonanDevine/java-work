package com.conygre.Accounts;

public class TestAccount {
    public static void main(String[] args) throws DodgyNameException {
        Account myAccount = new CurrentAccount(null, 0, Currency.GBP);
        myAccount.setName("gyu");
        myAccount.setBalance(10000.00);
        System.out.println(myAccount.getName() + " has a balance of £" + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println(myAccount.getName() + " has a balance of £" + myAccount.getBalance());

        Account[] arrayOfAccounts;
        arrayOfAccounts = new Account[5];
        double[] amounts = {25.60,50,75,100,175};
        String[] names = {"Ronan","ygu","Sarah","Alice","Brook"};

        for(int i = 0; i<arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new CurrentAccount(null, i, Currency.GBP);
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);

            System.out.println(arrayOfAccounts[i].getName() + " has a balance of £" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("With interest " + arrayOfAccounts[i].getName() + " has a balance of £" + arrayOfAccounts[i].getBalance());


        }
    }
}