package com.conygre.Accounts;

public abstract class  Account implements Detailable{
    private String name;
    private double balance;
    private static double interestRate = 1.1;
    private Currency currency;

    public Account(String name, double balance, Currency currency ) throws DodgyNameException {
        if("Fingers".equals(name))
        throw new DodgyNameException();
        this.name = name;
        this.balance = balance;
        this.currency = currency;
    }
    
    public Account() throws DodgyNameException {
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DodgyNameException {
       if("Fingers".equals(name))
       throw new DodgyNameException();
        this.name = name;
    }

    // public double addInterest(){
    //     balance = getBalance();
    //     double interest = balance * interestRate;
    //     String strDouble = String.format("%.2f", interest);
    //     double twoDecInterest = Double.parseDouble(strDouble);
    //     setBalance(twoDecInterest);
        
    //     return twoDecInterest;
    // }
    public  void addInterest()
    {
        balance = balance *1.1;
    };

    public static void setInterestRate(double d){
        interestRate = d;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public boolean withdraw(double amount){
        boolean flag = false;
        if((balance - amount)>= 0){
            balance -= amount;
            flag = true;
            System.out.println("You have successfully withdrwaw £" + amount + ", your new balance is £" + balance );
        }else{
            System.out.println("You're skint to the teeth sir");
        }
        return flag;

    }
    public boolean withdraw(){
        return withdraw(20);
    }


	public String getDetails()
	{
		return "The name is " + getName() + " and the balance is " + getBalance();
	}}