package com.conygre.Accounts;

public class TestInterfaces {
    public static void main(String[] args) throws DodgyNameException {
        Detailable[] details = { 
            new SavingsAccount("Ronan",2, Currency.GBP),
            new CurrentAccount("Michael",4, Currency.GBP),
            new HomeInsurance(20,20,20)
        };
        for(int i =0; i<details.length;i++){
            System.out.println(details[i].getDetails());
            System.out.println("\n");
        }
    }
}