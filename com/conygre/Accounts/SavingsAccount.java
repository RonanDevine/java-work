package com.conygre.Accounts;

public class SavingsAccount extends Account{

    public SavingsAccount(String name, double balance, Currency currency) throws DodgyNameException {
        super(name, balance, currency);
    }

    @Override
    public void addInterest()
    {
        double balance = getBalance()*1.4;
         String strDouble = String.format("%.2f", balance);
         double twoDecInterest = Double.parseDouble(strDouble);
         setBalance(twoDecInterest);
        this.setBalance(twoDecInterest);
    }
    
}