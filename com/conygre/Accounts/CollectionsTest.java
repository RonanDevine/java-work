package com.conygre.Accounts;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class CollectionsTest {
    public static void main(String[] args) throws DodgyNameException {
		Set<Account> accounts = new HashSet<Account>();

        double[] amounts = {20,30,40,50};
        String[] names = {"Emmett", "Barry", "Michael", "Ronan"};

        Account savings;
        for(int i=0;i<4;i++)
        {
            savings = new SavingsAccount(null, i, Currency.GBP);
            savings.setName(names[i]);
            savings.setBalance(amounts[i]);
            accounts.add(savings);
        }
        Iterator<Account> iterator = accounts.iterator();
        while (iterator.hasNext())
        {
            Account nextAccount = iterator.next();
            System.out.println("The name is " + nextAccount.getName());
            System.out.println("The balance is " + nextAccount.getBalance());
            nextAccount.addInterest();
            System.out.println("The new balance is " + nextAccount.getBalance());
        }
        for (Account nextAccount: accounts){
            System.out.println("The name is " + nextAccount.getName());
            System.out.println("The balance is " + nextAccount.getBalance());
            nextAccount.addInterest();
            System.out.println("The new balance is " + nextAccount.getBalance());
        }
    }
}