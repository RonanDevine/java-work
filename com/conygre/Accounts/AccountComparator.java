package com.conygre.Accounts;

import java.util.Comparator;

public class AccountComparator implements Comparator<Account> {

    @Override
    public int compare(Account a, Account b) {
        if (a.getBalance() < b.getBalance()) {
            return -1;
        } else if (a.getBalance() > b.getBalance()) {
            return 1;
        } else
            return 0;
    }

}
