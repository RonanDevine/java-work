package com.conygre.Accounts;

public class CurrentAccount extends Account{

    public CurrentAccount(String name, double balance, Currency currency) throws DodgyNameException {
        super(name, balance, currency);
    }
    @Override
    public void addInterest()
    {
        double balance = getBalance()*1.1;
        String strDouble = String.format("%.2f", balance);
        double twoDecInterest = Double.parseDouble(strDouble);
        setBalance(twoDecInterest);
       this.setBalance(twoDecInterest);
    }
    
}