package com.conygre.Accounts;

public class TestInheritance {
    public static void main(String[] args) throws DodgyNameException {
        Account[] arrayOfAccounts = { 
            new SavingsAccount("Ronan",2, Currency.GBP),
            new CurrentAccount("Michael",4, Currency.GBP),
            new SavingsAccount("Barry",6, Currency.GBP)};
        for(int i =0; i<arrayOfAccounts.length;i++){
            arrayOfAccounts[i].addInterest();
            System.out.println("The name is " + arrayOfAccounts[i].getName());
            System.out.println("The balance is " + arrayOfAccounts[i].getBalance());
        }
    }
    
}