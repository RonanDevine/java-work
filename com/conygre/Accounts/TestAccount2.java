package com.conygre.Accounts;

public class TestAccount2 {
    public static void main(String[] args) throws DodgyNameException {
        Account[] arrayOfAccounts;
        arrayOfAccounts = new Account[5];
        double[] amounts = {25.60,50,75,100,175};
        String[] names = {"Ronan","Eoin","Sarah","Alice","Brook"};
        Account.setInterestRate(1.4);
        for(int i = 0; i<arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new SavingsAccount(names[i], amounts[i], Currency.GBP);

            System.out.println(arrayOfAccounts[i].getName() + " has a balance of £" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("With interest " + arrayOfAccounts[i].getName() + " has a balance of £" + arrayOfAccounts[i].getBalance());

        }
        arrayOfAccounts[1].withdraw(20);

    }
}