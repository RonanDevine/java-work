import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TestStrings {
    public static void main(String[] args) {
        StringBuilder fileName = new StringBuilder("example.doc");
        fileName.replace(fileName.length() - 3, fileName.length(), "bak");

        String name1 = "Ronan";
        String name2 = "Serene";
        if (name1 == name2) {
            System.out.println("The same");
        } else if (name1.compareTo(name2) > 0) {
            System.out.println(name1 + " is a greater string");
        } else if (name2.compareTo(name1) > 0) {
            System.out.println(name2 + " is a greater string");
        } else{
            System.out.println("Something went wrong");
        }
        String text = "the quick brown fox swallowed down the lazy chicken";
        int count =0;
        for(int i=0; i <text.length()-2;i++){
            if(text.substring(i, i+2).equals("ow")){
                count++;
            }
            
        }
        System.out.println("The text says ow " + count + " times");
    
        String a, b ="";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the string you want to check:");
        a = scanner.nextLine();
        int n = a.length();
        for(int i = n - 1; i >= 0; i--)
        {
            b = b + a.charAt(i);
        }
        if(a.equalsIgnoreCase(b))
        {
            System.out.println("The string is a palindrome");
        }else{
            System.out.println("The string isn't a palindrome");
        }
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        Format monthDayYear = new SimpleDateFormat("MM dd yyyy");
        Format dayMonthShortYear = new SimpleDateFormat("dd MM yy");
        Format dayWordMonthYear = new SimpleDateFormat("dd MMMM yyyy");
        Format dayWordMonthShortYear = new SimpleDateFormat("dd MMMM yyyy");
        System.out.println(dayMonthYear.format(new Date()));
        System.out.println(monthDayYear.format(new Date()));
        System.out.println(dayMonthShortYear.format(new Date()));
        System.out.println(dayWordMonthYear.format(new Date()));
        System.out.println(dayWordMonthShortYear.format(new Date()));
    }


}
