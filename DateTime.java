import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Period;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;




public class DateTime {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in); // Create a Scanner object
        Scanner scanner = new Scanner(System.in);
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(1998, Month.MAY, 29);
        LocalDate nextBday = birthday.withYear(today.getYear());

        if (nextBday.isBefore(today) || nextBday.isEqual(today)) {
            nextBday = nextBday.plusYears(1);
        }
        Period p = Period.between(today, nextBday);
        System.out.println(
                "There are " + p.getDays() + "  days " + "and " + p.getMonths() + " months until your next birthday" );

        Format dayMonthYear = new SimpleDateFormat("dd/MM/yyyy hh mm ss");
        Calendar calNewYork = Calendar.getInstance();
        calNewYork.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        System.out.println(
                "Time in New York: " + calNewYork.get(Calendar.HOUR_OF_DAY) + ":" + calNewYork.get(Calendar.MINUTE)
                        + ":" + calNewYork.get(Calendar.SECOND) + " " + dayMonthYear.format(new Date()));

        Format minutes = new SimpleDateFormat("hh mm ss");

        Calendar now = Calendar.getInstance();
        int currentDays = now.get(Calendar.DAY_OF_WEEK);

        Calendar work = Calendar.getInstance();

        LocalDateTime dateTime = LocalDateTime.of(2020, 8, 26, 17, 00);
        LocalDateTime dateTime2 = LocalDateTime.now();
        long diffInMinutes = java.time.Duration.between(dateTime2, dateTime).toMinutes();
        long diffInHours = java.time.Duration.between(dateTime2, dateTime).toHours();
        System.out.printf("\nDifference is %d Hours or %d Minutes, \n\n", diffInHours, diffInMinutes);

        
        Calendar c = Calendar.getInstance();
        c.set(2020,8,27);
      //  LocalDate birthday = LocalDate.of(1998, Month.MAY, 29);

       DayOfWeek  dayOfWeek = ((GregorianCalendar) c).toZonedDateTime().getDayOfWeek();
        System.out.println(dayOfWeek);

        for(int i =1; i <=12;i++){
            Date dateTime3 = new Date(2021 + 1, i, 13);
            if (dateTime2.getDayOfWeek() == DayOfWeek.FRIDAY){
                System.out.println("dd MMMM yyyy" + "is a Friday");

            }

        }
        System.out.println("\nNext Friday: "+dateTime2.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)));
    System.out.println("Previous Friday: "+dateTime2.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY))+"\n");

        
    }
}