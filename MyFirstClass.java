import java.util.Scanner;

import javax.lang.model.util.ElementScanner6;

public class MyFirstClass {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in); // Create a Scanner object
        String make = "Volkswagen";
        String model = "Golf";
        double engineSize = 1.6;
        byte gear = 3;
        short speed = (short) (gear * 20);
        int revs = speed + 1000;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The speed is " + speed);
        System.out.println("The revs is " + revs);
        System.out.println("The engine size is " + engineSize);
        if (engineSize > 2.0) {
            System.out.println("The car is powerful");
        } else if (engineSize < 2.0 && engineSize > 1.4) {
            System.out.println("The car is average");
        } else
            System.out.println("You're driving a bean tin lad");
        System.out.println("Enter Gear");
        gear = myObj.nextByte();
        System.out.println("This is using a switch case");
        switch (gear) {
            case -1:
                System.out.print("Backwards");
                break;
            case 0:
                System.out.print("0mph");
                break;
            case 1:
                System.out.print("0-10mph");
                break;
            case 2:
                System.out.print("10-25mph");
                break;
            case 3:
                System.out.print("25-35mph");
                break;
            case 4:
                System.out.print("35-50mph");
                break;
            case 5:
                System.out.print("over 50mph");
                break;
            default:
                System.out.println("What kind of car do you drive!!?");
        }
        int count = 0;
        for (int i = 1900; i <= 2000; i++) {
            if ((i % 4) == 0) {
                count++;
                System.out.println("\n" + i + " is a leapyear.");
            }
            if (count == 5)
                break;
        }

    }
}
